async function findIp() {
    try {
        let response = await fetch('https://api.ipify.org/?format=json'); 
    let { ip } = await response.json();  
 
    let data = await fetch(`http://ip-api.com/json/${ip}`); 
    let userData = await data.json(); 
        console.log(userData);
        const infoIp= document.querySelector('.info');
        infoIp.innerHTML = `
            <h2>Інформація про IP</h2>
            <p>Континент: ${userData.continent}</p>
            <p>Країна: ${userData.country}</p>
            <p>Регіон: ${userData.regionName}</p>
            <p>Місто: ${userData.city}</p>
            <p>Район: ${userData.district}</p>
        `;
    } catch (err) {
        console.error('Error when receiving an IP address', err);
    }
}

document.querySelector('.butt').addEventListener('click', findIp);
